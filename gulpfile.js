var gulp = require('gulp'),
		jade = require('gulp-jade'),
		stylus = require('gulp-stylus'),
		watch = require('gulp-watch'),
		connect = require('gulp-connect'),
		prefix = require('gulp-autoprefixer'),
		uglify = require('gulp-uglify'),
		concat = require('gulp-concat'),
		rename = require('gulp-rename'),
		concatCSS = require('gulp-concat-css'),
		minify = require('gulp-minify-css');


	function handleError(err){
		console.log(err.toString());
		this.emit('end');
	}


	const public = './public/';
	const source = './source/';
	const pathJade = [
		source + 'jade/*.jade',
		'!' + source + 'jade/_*.jade',
		'!' + source + 'jade/external/_*.jade',
		'!' + source + 'jade/addons/_*.jade'
	];
	const pathStylus = [
		source + 'stylus/*.styl',
		'!' + source + 'stylus/_*.styl',
		'!' + source + 'stylus/addons/_*.styl'
	];
	const pathJS = [
		source + 'js/*.js'
	];
	const pathImages = source + 'images/**/*.*';

	const jadeWatchPath = [source + 'jade/*.jade', source + 'jade/addons/_*.jade', source + 'jade/_*.jade', source + 'jade/external/*_.jade'];
	const stylusWatchPath = [source + 'stylus/*.styl', source + 'stylus/_*.styl', source + 'stylus/addons/_*.styl'];
	const jsWatchPath = [source + 'js/*.js'];

	gulp.task('connect', function(){
		connect.server({
			root: public,
			port: 8001,
			livereload: true,
		});
	});

	gulp.task('jade', function(){
		return gulp.src(pathJade)
			.pipe(jade({pretty: true})).on('error', handleError)
			.pipe(gulp.dest(public))
			.pipe(connect.reload())
	});

	gulp.task('stylus', function(){
		return gulp.src(pathStylus)
			.pipe(stylus()).on('error', handleError)
			.pipe(concatCSS('all.css'))
			.pipe(prefix({
				browsers: ['last 3 versions'],
				cascade: false
			}))
			.pipe(minify())
			.pipe(rename('meltzer.css'))
			.pipe(gulp.dest(public + 'css'))
			.pipe(connect.reload())
	});

	gulp.task('js', function(){
		return gulp.src(pathJS)
			.pipe(uglify().on('error', handleError))
			.pipe(concat('script.js').on('error', handleError))
			.pipe(rename({suffix: '.min'}))
			.pipe(gulp.dest(public + 'js/'))
			.pipe(connect.reload());
	});

	gulp.task('fonts', function(){
		return gulp.src(source + 'fonts/**')
			.pipe(gulp.dest(public + 'fonts/'))
			.pipe(connect.reload());
	});

	gulp.task('favicon', function(){
		return gulp.src(source + '/*.ico')
			.pipe(gulp.dest(public))
			.pipe(connect.reload());
	});

	gulp.task('images', function(){
		return gulp.src(source + 'images/**/*')
			.pipe(gulp.dest(public + 'images/'))
			.pipe(connect.reload());
	});

	gulp.task('vendor:copy', function(){
		return gulp.src(source + 'vendor/**')
			.pipe(gulp.dest(public + 'vendor/'))
			.pipe(connect.reload());
	});

	gulp.task('copy:php', function() {
		return gulp.src(source + '*.php')
			.pipe(gulp.dest(public))
			.pipe(connect.reload());
	});


	gulp.task('pdf', function () {
		return gulp.src(source + 'pdf/**/*')
			.pipe(gulp.dest(public + 'pdf'))
			.pipe(connect.reload())
	});

	gulp.task('stream', function(){
		watch(source + 'vendor/**', function(event, cb){
			gulp.start('vendor:copy')
		});
		watch(source + 'pdf/**/*', function (event, cb) {
			gulp.start('pdf')
		});
		watch(jadeWatchPath, function(event, cb){
			gulp.start('jade')
		});
		watch(stylusWatchPath, function(event, cb){
			gulp.start('stylus')
		});
		watch(jsWatchPath, function(event, cb){
			gulp.start('js')
		});
		watch(source + 'fonts/**', function(event, cb){
			gulp.srart('fonts')
		});
		watch(source + 'images/**/*', function(event, cb){
			gulp.start('images')
		});
		watch(source + 'php/*.php', function(event, cb){
			gulp.start('copy:php')
		});
	});


	gulp.task('default', ['vendor:copy', 'connect', 'pdf', 'copy:php','favicon', 'images' ,'jade', 'js', 'stylus', 'fonts', 'stream']);