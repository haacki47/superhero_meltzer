ymaps.ready(function(){

	 ymaps.ready(init);
        function init () {
            /* Создание экземпляра карты и его привязка к контейнеру с заданным id ("map") */
            var myMap = new ymaps.Map('map-contact', {
                    /* При инициализации карты, обязательно нужно указать ее центр и коэффициент масштабирования */
                    center: [55.761639,37.657011], // Нижний Новгород
                    zoom: 15,
                    behaviors: ["scrollZoom", "drag"]
                });
     myMap.behaviors.disable("scrollZoom");
     myMap.behaviors.enable("drag");
		var geometry = [[55.761633,37.658525], [55.761592,37.657580], [55.758586,37.657858], [55.758554,37.658936]],
			properties = {
				hintContent: "От метро Курская"
			},
			options = {
				draggable: true,
				strokeColor: '#ff0000',
				strokeWidth: 5
			},
			polyline = new ymaps.Polyline(geometry, properties, options);
			var myPlacemark = new ymaps.Placemark([55.761633,37.658525], {
						iconContent: 'M',
						balloonContentBody: '<p>MELTZER LITIGATION & CONSULTING</p><img src="images/mapimage.jpg"  alt""> <p>г. Москва, ул. Земляной вал, д. 9</p>'
			});
			myMap.geoObjects.add(polyline);				
			myMap.geoObjects.add(myPlacemark);				
 			myMap.balloon.open(
				// Позиция балуна
				[55.761633,37.658525], {
				contentBody: '<p>MELTZER LITIGATION & CONSULTING</p><img src="images/mapimage.jpg"  alt""> <p>Г. Москва, ул. Земляной вал, д. 9</p>'
			}, {
               /* Опции балуна. В данном примере указываем, что балун не должен иметь кнопку закрытия. */
                closeButton: true
        });

	}


});