$(document).ready(function(){
	var getModalContact = $('#contactModal'),
			buttonOpenContact = $('.modal-contact-button-open');

				buttonOpenContact.on('click', function(){
					getModalContact.modal('show');
					var interval = setInterval(function() {
						$('#idNameContact').focus();
					}, 1000);
					setInterval(function () {
						clearInterval(interval);
					}, 1100);
				});
	var getModalPop = $('#modalPopup'),
			openPop = $('.open-path-pop');

	if ( openPop ) {
		openPop.click(function () {
			getModalPop.modal('show');
		});
	}


});