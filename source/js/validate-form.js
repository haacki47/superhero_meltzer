$(document).ready(function () {

	var nedvFirstValidate = $('#formValidateNedv'),
			nedvSecondValidate = $('#formValidateNedvFirst'),
			fileForm = $('#requiredFileForm');

		$('#formodalValidate').validate({
			rules: {
				phone: {
					required: true,
					minlength: 3
				}
			},
			submitHandler: function(form) {
				$.ajax({
					url: './form-submit.php',
					type: 'text',
					data: $('#formodalValidate').serialize(),
					success: function(data){
						console.log("VALID!");
					},
					error: function() {
						console.log("ERROR");
					}
				});
			}
		});

	if(fileForm){
		fileForm.validate({
			rules: {
				email: {
					required: true,
					email: true
				}
			}
		});
	}

	if(nedvSecondValidate){
		console.log(1);
		nedvSecondValidate.validate({
			rules: {
				phone: {
					required: true,
					minlength: 3
				}
			}
		})
	}

	if(nedvFirstValidate){
		nedvFirstValidate.validate({
			rules: {
				phone: {
					required: true,
					minlength: 3
				}
			}
		});
	}

});